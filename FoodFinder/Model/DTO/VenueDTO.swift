//
//  VenueDTO.swift
//  FoodFinder
//
//  Created by Per Friis on 22/03/2018.
//  Copyright © 2018 Per Friis Consult ApS. All rights reserved.
// code generated https://app.quicktype.io with some motification, in UUID and date fields
//
//
// To parse the JSON, add this file to your project and do:
//
//   let venuesDto = try VenuesDto(json)

import Foundation

typealias VenuesDto = [VenueDto]

struct VenueDto: Codable {
    let id: UUID?
    let version: Int64
    let created: Date?
    let deleted: Bool
    let title: String?
    let detail: String?
    let type: Int16
    let logoImaageURL: URL?
    let street: String?
    let zipCode: String?
    let city: String?
    let state: String?
    let country: String?
    let email: String?
    let phone: String?
    let latitude: Double
    let longitude: Double
    let priceLevel: Double
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case version = "version"
        case created = "created"
        case deleted = "deleted"
        case title = "title"
        case detail = "detail"
        case type = "type"
        case logoImaageURL = "logoImaageUrl"
        case street = "street"
        case zipCode = "zipCode"
        case city = "city"
        case state = "state"
        case country = "country"
        case email = "email"
        case phone = "phone"
        case latitude = "latitude"
        case longitude = "longitude"
        case priceLevel = "priceLevel"
    }
}

// MARK: Convenience initializers

extension VenueDto {
    init(data: Data) throws {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601

        self = try decoder.decode(VenueDto.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        
        return try encoder.encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == VenuesDto.Element {
    init(data: Data) throws {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        self = try decoder.decode(VenuesDto.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        
        return try encoder.encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
