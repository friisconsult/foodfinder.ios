//
//  Venue.swift
//  FoodFinder
//
//  Created by Per Friis on 22/03/2018.
//  Copyright © 2018 Per Friis Consult ApS. All rights reserved.
//

import CoreData
import MapKit

extension Venue {
    /**
     A codable struct, to use as communication with backend or other types that needs
     JSON data.
     - notes: This is implemented as the NSManagedObject don't support codable protocol.
    */
    var dto:VenueDto {
        get {
            return VenueDto(id: id,
                     version: version,
                     created: created,
                     deleted: false,
                     title: title,
                     detail: detail,
                     type: typeId,
                     logoImaageURL: logoImageUrl,
                     street: street,
                     zipCode: zipCode,
                     city: city,
                     state: state,
                     country: country,
                     email: email,
                     phone: phone,
                     latitude: latitude,
                     longitude: longitude,
                     priceLevel: priceLevel)
        }
        set {
            guard version != newValue.version else {
                return
            }
            id = newValue.id
            version = newValue.version
            created = newValue.created
            title = newValue.title
            detail = newValue.detail
            typeId = newValue.type
            logoImageUrl = newValue.logoImaageURL
            street = newValue.street
            zipCode = newValue.zipCode
            city = newValue.city
            country = newValue.country
            email = newValue.email
            phone = newValue.phone
            latitude = newValue.latitude
            longitude = newValue.longitude
            priceLevel = newValue.priceLevel
        }
    }
    
    
    public var type:String {
        switch typeId {
        case 0:
            return NSLocalizedString("Indien food", comment: "venue type")
            
        default:
            return NSLocalizedString("Other", comment: "venue type")
        }
    }
}

// MARK: - class functions
extension Venue {
    /**
     find a list of venues that matches the predicate, in the context
     - Parameters:
        - predicate: The filter, if nil, all venues is returned
        - context: the NSmanagedObjectContext to use
    
     - Returns: an array of Venues, can be empty
    */
    static func find(predicate:NSPredicate = NSPredicate(format:"TRUEPREDICATE"),context:NSManagedObjectContext) -> [Venue] {
        let fetchRequest:NSFetchRequest<Venue> = self.fetchRequest()
        fetchRequest.predicate = predicate
        
        do {
            return try context.fetch(fetchRequest)
        } catch {
            assertionFailure(error.localizedDescription)
        }
        return []
        
    }
    
    /**
     find a venue with the ID
     - Parameters:
        - id: the uuid of the venue to find
        - context: the managed object context to use
     - Returns: the Venue if exists otherwise nil
    */
    static func find(id:UUID, context:NSManagedObjectContext) -> Venue? {
        return self.find(predicate: NSPredicate(format:"id == %@",id.uuidString), context: context).first
    }
    
    /**
     Creates a new and/or update a Venue, based on the Venue.ID
     - Note: If the dto.version is the same as the existing venue, no data is updated.
     
     The context is not saved
     
     - Parameters:
        - dto: The codable DTO for Venues, with the data to update
        - context: In the context to use
     - Returns: returns the updated venue
    */
    static func createOrUpdate(dto:VenueDto, context:NSManagedObjectContext) -> Venue {
        let venue = find(id: dto.id!, context: context) ?? Venue(context: context)
        
        venue.dto = dto

        return venue
    }
}


/**
 Implement support for mapkit
 */
extension Venue:MKAnnotation {
    public var coordinate: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        set {
            latitude = newValue.latitude
            longitude = newValue.longitude
        }
    }
    
    public var subtitle: String? {
        return type
    }
}
