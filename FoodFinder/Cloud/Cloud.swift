//
//  Cloud.swift
//  FoodFinder
//
//  Created by Per Friis on 22/03/2018.
//  Copyright © 2018 Per Friis Consult ApS. All rights reserved.
//

import Foundation
import UIKit
import CoreData

/// Completion block used by the Cloud class functions
typealias CloudCompletionBlock = (_ success:Bool,_ error:Error? ) -> Void

/**
 The cloud class holds all the communication to the backend.
 The cloud class is a Singleton, that makes sure to have a valid token to the backend
 at all time.
 
 - Note:
 Use the static *shared*
 */
class Cloud: NSObject {
    static let share:Cloud = Cloud()
    
    var persistentContainer:NSPersistentContainer!

    /// the base url to the api
    let apiUrl = URL(string:"https://foodfinderapi.azurewebsites.net/api/v1/")!
    
    
    func fetchVenues(complete:@escaping CloudCompletionBlock) {
        guard persistentContainer != nil else {
            complete(false,nil)
            return
        }
        var request = URLRequest(url: apiUrl.appendingPathComponent("venues"))
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("bearer laksdfjlasjfls jdf", forHTTPHeaderField: "Authorization")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, urlResponse, error) in
            guard error == nil else {
                assertionFailure(error!.localizedDescription)
                complete(false, error)
                return
            }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                complete(false, nil)
                return
            }
            
            guard httpResponse.statusCode == 200 else {
                complete(false, nil)
                return
            }
            
            guard let data = data else {
                complete(false, NSError(domain: "foodfinder", code: -1, userInfo: ["descriptiop":"Data was nil"]))
                return
            }
            self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
            self.persistentContainer.performBackgroundTask({ (context) in
                
                do {
                    
                    
                    let venuesDto = try VenuesDto(data:data)
                    
                    for venueDto in venuesDto {
                        
                        let _ = Venue.createOrUpdate(dto: venueDto, context: context)
                        
                    }
                    if context.hasChanges {
                        try context.save()
                    }
                } catch {
                    assertionFailure(error.localizedDescription)
                    complete(false, error)
                }
            })
        }
        
        task.resume()
        
    }
    
}
