//
//  VenuesTableViewController.swift
//  FoodFinder
//
//  Created by Per Friis on 22/03/2018.
//  Copyright © 2018 Per Friis Consult ApS. All rights reserved.
//

import UIKit
import CoreData


class VenuesTableViewController: UITableViewController {
    lazy var fetchedResultsController:NSFetchedResultsController<Venue> = {
        let fetchRequest:NSFetchRequest<Venue> = Venue.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "TRUEPREDICATE")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key:"typeId", ascending:true),
                                        NSSortDescriptor(key:"title", ascending:true)]
        
        let fetchedResultsController:NSFetchedResultsController<Venue> = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext, sectionNameKeyPath: "typeId", cacheName: nil)
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError(error.localizedDescription)
        }
        return fetchedResultsController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        title = NSLocalizedString("Venues", comment: "navigation bar title")
        
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "venue cell", for: indexPath)
        let venue = fetchedResultsController.object(at: indexPath)
        
        cell.textLabel?.text = venue.title
        cell.detailTextLabel?.text = venue.detail
        
        return cell
    }
    
    
    
    // MARK:- navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var venue:Venue?
        
        if let cell = sender as? UITableViewCell,
            let indexpath = tableView.indexPath(for: cell) {
            venue = fetchedResultsController.object(at: indexpath)
        }
        
        
        if let venueDetailsViewController = segue.destination as? VenueDetailsTableViewController {
            venueDetailsViewController.venue = venue
        }
    }
}

// MRAK:- FetchedResultsConstrollerDelegate
extension VenuesTableViewController:NSFetchedResultsControllerDelegate {
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String? {
        return sectionName + " type"
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        let indexSet = IndexSet([sectionIndex])
        switch type {
        case .insert:
            tableView.insertSections(indexSet, with: .automatic)
            
        case .update:
            tableView.reloadSections(indexSet, with: .automatic)
            
        case .delete:
            tableView.deleteSections(indexSet, with: .automatic)
            
        default:
            tableView.reloadSectionIndexTitles()
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
            
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
            
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
            
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
