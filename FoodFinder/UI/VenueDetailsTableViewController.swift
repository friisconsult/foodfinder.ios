//
//  VenueDetailsTableViewController.swift
//  FoodFinder
//
//  Created by Per Friis on 23/03/2018.
//  Copyright © 2018 Per Friis Consult ApS. All rights reserved.
//

import UIKit
import MapKit

class VenueDetailsTableViewController: UITableViewController {
    @IBOutlet weak var mapView:MKMapView!
    @IBOutlet weak var detailLabel:UILabel!
    
    @IBOutlet weak var streetLabel:UILabel!
    @IBOutlet weak var zipLabel:UILabel!
    @IBOutlet weak var cityLabel:UILabel!
    @IBOutlet weak var stateLabel:UILabel!
    @IBOutlet weak var countryLabel:UILabel!
    
    @IBOutlet weak var phoneLabel:UILabel!
    @IBOutlet weak var emailLabel:UILabel!

    
    var venue:Venue?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        
        updateUI()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row == 3
    }

    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard indexPath.row == 3 else {
            return nil
        }
        
        return [
            UITableViewRowAction(style: .default, title: "Call", handler: { (action, indexpath) in
                // TODO: Call the venue
            }),
            UITableViewRowAction(style: .normal, title: "e-mail", handler: { (action, index) in
                // TODO: start an email
            })
        ]
        
    }

   
    func updateUI(){
        guard let venue = venue else {
            return
        }
        title = venue.title
        
        mapView.addAnnotation(venue)
        mapView.showAnnotations(mapView.annotations, animated: true)
        
        detailLabel.text = venue.detail
        
        streetLabel.text = venue.street
        zipLabel.text = venue.zipCode
        cityLabel.text = venue.city
        stateLabel.text = venue.state
        countryLabel.text = venue.country
        
        phoneLabel.text = venue.phone
        emailLabel.text = venue.email
        
    }

}
